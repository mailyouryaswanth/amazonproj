import React from "react";
import { DropdownButton, Dropdown } from "react-bootstrap";
import { HiOutlineLocationMarker } from "react-icons/hi";
import { BsSearch } from "react-icons/bs";
import { FaOpencart } from "react-icons/fa";
import { MdArrowDropDown } from "react-icons/md";
import NavbarB from "./NavbarB";
import Banner from "./Banner";
import OverlayCards from "./components/OverlayCards";

const NavBar = () => {
  return (
    <>
      <nav class="navbar navbar-expand-lg navbar1">
        <div class="container-fluid">
          <div className=" Box">
            <img
              src="https://logos-world.net/wp-content/uploads/2020/04/Amazon-Emblem.jpg"
              alt=""
              width="100"
              height="50"
            />
          </div>
          &nbsp;
          <div class="collapse navbar-collapse">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <div className="Box">
                  <label>
                    <HiOutlineLocationMarker /> Hello, Select your address
                  </label>
                </div>
              </li>
            </ul>
            <form class="container-fluid">
              <div class="input-group">
                <DropdownButton variant="secondary" title="All" align="end">
                  <Dropdown.Item>Action</Dropdown.Item>
                </DropdownButton>
                <input type="text" class="form-control" />
                <button type="button" class="btn btn-warning">
                  <BsSearch />
                </button>
              </div>
            </form>
          </div>
          <div className="country">
                    <div class="input-group">
                        <img src="https://upload.wikimedia.org/wikipedia/en/thumb/4/41/Flag_of_India.svg/1200px-Flag_of_India.svg.png" className="CountryFlag"></img>
                    </div>
                    <div class="dropdown">
                        <button class="dropbtn"><MdArrowDropDown className="Arrow" /></button>
                        <div class="dropdown-content">
                            <a href="#">Link 1</a>
                            <a href="#">Link 2</a>
                            <a href="#">Link 3</a>
                        </div>
                    </div>
                </div>

                <div className="nav-up-right">
                    <div className="nav-right">
                        <div className="input-group">
                            <div className="sign-up">
                                <span>Hello,Sign in</span><br></br>
                                <div className="lists">
                                    <h6>Accounts & Lists</h6>
                                </div>
                            </div>
                        </div>
                        <div class="dropdown">
                            <button class="dropbtn1"><MdArrowDropDown className="Arrow" /></button>
                            <div class="dropdown-content">
                                <a href="#">Link 1</a>
                                <a href="#">Link 2</a>
                                <a href="#">Link 3</a>
                            </div>
                        </div>
                    </div>

                    <div className="orders">
                        <div className="returns">
                            <span>Returns</span>
                        </div>
                        <div className="returnOrders">
                             <h6>& Orders</h6>
                        </div>
                    </div>
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
             
                <FaOpencart size={40}/> Cart
              
            </li>
          </ul>
        </div>
        </div>
      </nav>
      {/* ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */}
     
      <NavbarB/>
      <Banner />
      <OverlayCards/>
    </>
  );
};

export default NavBar;
