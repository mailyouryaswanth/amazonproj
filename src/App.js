import "./App.css";
import NavBar from "./NavBar";
import OverlayCards  from "./components/OverlayCards";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<NavBar />} />{" "}
        {/* <Route path="/" element={<OverlayCards />} />{" "} */}
        </Routes>
      </Router>
    </div>
  );
}

export default App;
