import React from "react";
import "./OverlayCards.css"
import {Card} from 'react-bootstrap'

const OverlayCards = () => {
  return (
    <Card  style={{ width: '21.5rem',borderColor:"transparent" }}>  
    <Card.Body>
      <Card.Title style={{textAlign:"left",fontWeight:"bold",fontSize:"21px!important;"}}>Amazon Pay | Book your travel tickets</Card.Title>
      <Card.Text>
        <div id="grid-container">
          <div id="grid-item">
            <div>
         <img src="https://images-eu.ssl-images-amazon.com/images/G/31/img16/AmazonPayTravels/November/GWPercolate/Flight_372x232._SY232_CB653435429_.jpg" style={{height:"100px",width:"148px"}}/>
        </div>
        <div id="textCard">
          Flight tickets
          </div>
        </div>
        <div id="grid-item">
          <div>
         <img src="https://images-eu.ssl-images-amazon.com/images/G/31/img16/AmazonPayTravels/November/GWPercolate/Bus_372x232._SY232_CB653435429_.jpg" style={{height:"100px",width:"148px"}}/>
        </div>
        <div id="textCard">
          Bus tickets
          </div>
        </div>
    
        <div id="grid-itemOne">
            <div>
         <img src="https://images-eu.ssl-images-amazon.com/images/G/31/img16/AmazonPayTravels/November/GWPercolate/Train_372x232._SY232_CB653435429_.jpg" style={{height:"100px",width:"148px"}}/>
        </div>
        <div id="textCard">
          Train tickets
          </div>
        </div>
        <div id="grid-itemOne">
          <div>
         <img src="https://images-eu.ssl-images-amazon.com/images/G/31/img16/AmazonPayTravels/November/GWPercolate/essential_372x232._SY232_CB653435429_.jpg" style={{height:"100px",width:"148px"}}/>
        </div>
        <div id="textCard">
          Essential travel products
          </div>
        </div>
        </div>
        <div className="hyPer">
       <a href="" id="hyPer" style={{fontSize:"12px",color:"#007185",textDecoration:"none"}}>Explore more from Amazon Pay</a>
        </div>
      </Card.Text>
    </Card.Body>
  </Card>
  
  );
};

export default OverlayCards;
