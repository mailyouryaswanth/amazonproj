import React from "react";
import './NavbarB.css'
import { BiMenu } from "react-icons/bi";
import { AiFillAmazonSquare } from "react-icons/ai";
const NavbarB = () => {
  return (
    <nav class="navbar navbar-expand-lg ">
    <div class="container-fluid">
      <button type="button" class="btn ">
        {" "}
        <BiMenu size={35} /> All
      </button>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <button type="button" class="btn ">
              Best Sellers
            </button>
          </li>
          <li class="nav-item">
            <button type="button" class="btn ">
              Mobiles
            </button>
          </li>
          <li class="nav-item">
            <button type="button" class="btn ">
              Customer Service
            </button>
          </li>
          <li class="nav-item">
            <button type="button" class="btn ">
              Today's Deals
            </button>
          </li>
          <li class="nav-item">
            <button type="button" class="btn ">
              Electronics
            </button>
          </li>
          <li class="nav-item">
            <button type="button" class="btn ">
              Fashion
            </button>
          </li>

          <li class="nav-item">
            <button type="button" class="btn ">
              Prime
            </button>
          </li>
          <li class="nav-item">
            <button type="button" class="btn ">
              Books
            </button>
          </li>
          <li class="nav-item">
            <button type="button" class="btn ">
              New Releases
            </button>
          </li>
          <li class="nav-item">
            <button type="button" class="btn ">
              Home & Kitchen
            </button>
          </li>
          <li class="nav-item">
            <button type="button" class="btn ">
              Amazon Pay
            </button>
          </li>
          <li class="nav-item">
            <button type="button" class="btn ">
              Computers
            </button>
          </li>
          <li class="nav-item">
            <button type="button" class="btn ">
              Coupons
            </button>
          </li>
        </ul>
        <form class="d-flex nav-item">
          <button type="button" class="btn ">
            <AiFillAmazonSquare size={35} />
            Shopping made easy | Download the app
          </button>
        </form>
       
      </div>
    </div>
  </nav>
  );
};

export default NavbarB;
